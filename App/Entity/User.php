<?php

namespace App\Entity;

class User
{
    private $firstName;
    private $lastName;
    private $email;
    private $userName;
    private $dob;
    private $bio;
    private $password;


    public function setForSignUp(array $data){

        $this->firstName = $data['firstname'];
        $this->lastName = $data['lastname'];
        $this->email = $data['email'];
        $this->userName = $data['username'];
        $this->dob = $data['dob'];
        $this->password = $data['password'];
    }

    public function setForLogin(array $data){
        $this->email = $data['email'];
        $this->password = $data['password'];
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return mixed
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @return mixed
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }


}
