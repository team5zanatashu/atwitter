<?php


namespace App\Entity;


class Post
{
    private $body;
    private $image;

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }
    private $user_id;
//    private $picture;


    public function setPost($post)
    {
        $this->body = $post['post_field'];
        $this->image = $post['post_image'];
        $this->user_id = $post['post_user'];
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
//    public function getPicture()
//    {
//        return $this->picture;
//    }


}
