<?php


namespace App\Service;


class SessionAuth implements Auth
{
    public static function logIn($user) {
//        session_start();
        $_SESSION['isUserLogin'] = $user;
    }

    public static function logOut() {
//        session_start();
        unset($_SESSION['isUserLogin']);
        header('Location: /');
    }

    public static function isLoggedIn() {
//        session_start();
        if(!empty($_SESSION)) {
            return true;
        }
        return false;
    }

    public static function requireLogIn() {
        if (!self::isLoggedIn()) {
            header('Location: /');
            exit;
        }
    }
}
