<?php


namespace App\Service;


interface Auth
{
    public static function logIn($user);

    public static function logOut();

    public static function requireLogIn();

    public static function isLoggedIn();

}
