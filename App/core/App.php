<?php

namespace App\Core;

class App
{
    private static $db;
    private static $router;

    public static function start() {
        require dirname(__DIR__) . '/consts.php';

        self::$db = new Database();
        self::$router = new Router();
        self::$router->run();
    }

    public static function getDB() {
        return self::$db;
    }
}
