<?php

namespace App\Core;


use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

require_once dirname(__DIR__, 2).'/vendor/autoload.php';

class Controller {

//    private $userDAO;
//
//    public function __construct()
//    {
//        $this->userDAO = new UserDAO();
//    }

    private function renderPrepare() {
        $loader = new FilesystemLoader(ROOTPATH.'/views');

        $twig = new Environment($loader, [
            'cache' => false,
        ]);

        return $twig;
    }

    public function render($viewName, $params = []) {

        $twig = $this->renderPrepare();
        $params['localhost'] = LOCALHOST;

        try {
            echo $twig->render($viewName.'.twig', $params);
        } catch (LoaderError $e) {
            echo $e;
        } catch (RuntimeError $e) {
            echo $e;
        } catch (SyntaxError $e) {
            echo $e;
        }
    }

    public function jsonRender($viewName, $params = []) {

        $twig = $this->renderPrepare();
        $params['localhost'] = LOCALHOST;

        echo json_encode($twig->render($viewName.'.twig', $params));
    }
}
