<?php

namespace App\Core;

use App\Controllers\ErrorsController;

class Router
{
    private $routes = [];
    private $params = [];

    public function __construct(){
        $this->routesInit();
    }

    private function routesInit(){
        $arr = require dirname(__DIR__).'/config/routes.php';
        foreach ($arr as $key => $value) {
            $this->add($key, $value);
        }
    }

    private function add($route, $params){
        $route = '#^'.$route.'$#';
        $this->routes[$route] = $params;
    }

    private function match(){
        $url = trim($_SERVER['REQUEST_URI'], '/');
        $request = explode('/', $url);
        $i = $request[0] === 'api'? 1 : 0;
        $url = isset($request[$i + 1]) ? $request[$i].'/'.$request[$i + 1] : $request[$i];

        if(isset($request[$i + 2])) {
            $arg = ($request[$i + 2] == '') ? false : $request[$i + 2];
        }
        else {
            $arg = false;
        }
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url)) {
                $this->params = $params;
                if ($url == 'user/account') {
                    $this->params['arg'] = $arg;
//                    return true;
                }
//                else {
//                    if (isset($request[2])) {
//                        $loc = trim($route, '#$^');
//                        header('Location: '.LOCALHOST."$loc");
//                    }
                    return true;
//                }
            }
        }
        return false;
    }

    public function run(){
        if($this->match()) {
            $env = preg_match('/^api/', trim($_SERVER['REQUEST_URI'], '/')) ? 'Api' : 'App';
            $classname = $env.'\\Controllers\\'.ucfirst($this->params['controller']).'Controller';
            $action = $this->params['action'].'Action';
            $controller = new $classname();
            if(isset($this->params['arg'])) {
                $controller->$action($this->params['arg']);
            }
            else {
                $controller->$action();
            }
        }
        else {
            $controller = new ErrorsController();
            $controller->err404Action();
        }
    }
}
