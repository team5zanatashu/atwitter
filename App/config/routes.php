<?php

    return [
        '' => [
            'controller' => 'main',
            'action' => 'index',
        ],
        'user/login' => [
            'controller' => 'user',
            'action' => 'login',
        ],
        'user/signup' => [
            'controller' => 'user',
            'action' => 'signup',
        ],
        'user/account' => [
            'controller' => 'user',
            'action' => 'account',
        ],
        'user/edit' => [
            'controller' => 'user',
            'action' => 'edit',
        ],
        'user/homepage' => [
            'controller' => 'user',
            'action' => 'homepage',
        ],
        'user/search' => [
            'controller' => 'user',
            'action' => 'search',
        ],
        'user/logout' => [
            'controller' => 'user',
            'action' => 'logout',
        ],
        'user/update' => [
            'controller' => 'user',
            'action' => 'update',
        ],
        'user/password' => [
            'controller' => 'user',
            'action' => 'password',
        ],
        'post/create' => [
            'controller' => 'post',
            'action' => 'create'
        ],
        'post/like' => [
            'controller' => 'post',
            'action' => 'like'
        ],
        'post/unlike' => [
            'controller' => 'post',
            'action' => 'unlike'
        ],
        'post/delete'=>[
            'controller'=>'post',
            'action'=>'delete'
        ],
        'subscribe/change' => [
            'controller' => 'subscribe',
            'action' => 'change'
        ],
        'subscribe/check' => [
            'controller' => 'subscribe',
            'action' => 'check'
        ],
        'subscribe/followers' => [
            'controller' => 'subscribe',
            'action' => 'followers'
        ],
        'subscribe/following' => [
            'controller' => 'subscribe',
            'action' => 'following'
        ],
        'subscribe/friends' => [
            'controller' => 'subscribe',
            'action' => 'friends'
        ],
        'message/allchats' => [
            'controller' => 'message',
            'action' => 'allchats'
        ],
        'message/send' => [
            'controller' => 'message',
            'action' => 'send'
        ],
        'message/singlechat' => [
            'controller' => 'message',
            'action' => 'singlechat'
        ],
        'message/updatechat' => [
            'controller' => 'message',
            'action' => 'updatechat'
        ],
        'message/edit' => [
            'controller' => 'message',
            'action' => 'edit'
        ],
        'message/delete' => [
            'controller' => 'message',
            'action' => 'delete'
        ],
        'errors/comeback' => [
            'controller' => 'errors',
            'action' => 'comeback'
        ]
    ];
