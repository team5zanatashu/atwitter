<?php

namespace App\Controllers;

use App\Helpers\Iterator;
use App\Models\PostDAO;
use App\Models\UserDAO;
use App\Core\Controller;
use App\Entity\User;
use App\Helpers\Validation;
use App\Service\SessionAuth;


class UserController extends Controller {

    private $userDAO;
    private $postDAO;

    public function __construct() {
        $this->userDAO = new UserDAO();
        $this->postDAO = new PostDAO();
    }


    public function loginAction() {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST = Iterator::trimSpaces($_POST);
            $errors = Validation::validationForLogin($_POST);
            if(empty($errors)) {
                $user = new User();
                $user->setForLogin($_POST);
                $userFromDb = $this->userDAO->read(['email' => $user->getEmail()])[0];
                $user_id = $userFromDb['id'];
                $username = $userFromDb['username'];
                $checkHash = password_verify($user->getPassword(), $userFromDb['password']);
                if($user_id && $checkHash) {
                    SessionAuth::logIn($user_id);
                    header('Location: account/'.$username);
                }
                else {
                    echo 'User not found!';
                }
            }
            else {
                $this->render('login', [
                    'errors' => $errors,
                    'formdata' => $_POST
                ]);
            }
        }

        $this->render('login');
    }

    public function logoutAction() {
        SessionAuth::logOut();
    }

    public function signupAction(){
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST['dob'] = $_POST['dob_day']. '.' .$_POST['dob_month']. '.' .$_POST['dob_year'];

            $_POST = Iterator::trimSpaces($_POST);
            $errors = Validation::validationForSignUp($_POST);
            if (empty($errors)) {
                $user = new User();
                $user->setForSignUp($_POST);
                $checkEmail = $this->userDAO->read(['email' => $user->getEmail()]);
                $checkUserName = $this->userDAO->read(['username' => $user->getUserName()]);

                if (!$checkEmail && !$checkUserName) {
                    $this->userDAO->create($user);
                    SessionAuth::logIn($this->userDAO->read(['email' => $user->getEmail()])[0]['id']);
                    header('Location: account/'. $user->getUserName());
                }
                else {
                    echo 'Such user already exists';
                }
            }
            else {
                $this->render('signup', [
                    'errors' => $errors,
                    'formdata' => $_POST
                ]);
            }
        }
        else {
            $this->render('signup');
        }
    }


    public function accountAction(string $username = ''){
        SessionAuth::requireLogIn();
        $defaultUser = $this->userDAO->read(['id' => $_SESSION['isUserLogin']])[0]['username'];
        if($_SERVER['REQUEST_METHOD'] == "GET") {

            $user = $this->userDAO->read(['username' => $username])[0] ?? false;
            if(!$user) {
                header('Location: '.LOCALHOST.'user/account/'. $defaultUser);
                exit;
            }
            $userPosts = $this->postDAO->getAllPostsByUserId($user['id']);
            $userPosts = Iterator::postsIter($userPosts, $user);
            $userPosts = Iterator::likesIter($this->postDAO, $userPosts);
            $params = [
              'user' => $user,
              'userPosts' => $userPosts
            ];


            $this->render('account', $params);
        }
    }

    public function editAction(){
        SessionAuth::requireLogIn();
        $userDefault = $this->userDAO->read(["id" => $_SESSION['isUserLogin']])[0];
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['dob_day'])) {
                $_POST['dob'] = $_POST['dob_day']. '.' .$_POST['dob_month']. '.' .$_POST['dob_year'];
            }
            $_POST = Iterator::trimSpaces($_POST);

            $errors = Validation::validationForUpdate($_POST);
            if (isset($_POST['password']) && !password_verify($_POST['current-password'], $userDefault['password'])) {
                $errors["current_password"] =  "Invalid Password";
                //здесь можно добавить счетчик на неправильно введенный пароль и редиректить на начальную с логаутом
            }
            if (empty($errors)) {
                $tempFields = [];
                if (isset($_FILES['profileimg']) && $_FILES['profileimg']["name"]) {
                    $newdir = dirname(__DIR__, 2).'/public/img/avatars/'.$_SESSION['isUserLogin'].$_FILES["profileimg"]["name"];
                    move_uploaded_file($_FILES["profileimg"]["tmp_name"], $newdir);
                    $tempFields['profileimg'] = $_SESSION['isUserLogin'].$_FILES['profileimg']["name"];
                }
                if (isset($_FILES['background']) && $_FILES['background']["name"]) {
                    $newdir = dirname(__DIR__, 2).'/public/img/profile_backgrounds/'.$_SESSION['isUserLogin'].$_FILES["background"]["name"];
                    move_uploaded_file($_FILES["background"]["tmp_name"], $newdir);
                    $tempFields['background'] = $_SESSION['isUserLogin'].$_FILES['background']["name"];
                }
                foreach ($_POST as $key => $value) {
                    if (array_key_exists($key, $userDefault) && !preg_match('/^\s*$/', $value) &&
                        $value !== "day.month.year"){
                        $tempFields[$key] = $_POST[$key];
                    }
                }
                $checkEmail = $this->userDAO->read(['email' => $_POST['email']]);
                $checkUserName = $this->userDAO->read(['username' => $_POST['username']]);

                if (!$checkEmail && !$checkUserName) {
                    $this->userDAO->update($tempFields);
                    header('Location: '.LOCALHOST.'user/edit');
                }
                else {
                    echo 'Such user already exists';
                }
            }
            else {
                $this->render('edit', [
                    'user' => $userDefault,
                    'errors' => $errors,
                    'formdata' => $_POST
                ]);
            }
        }
        else {
            $this->render('edit', ['user' => $userDefault]);
        }
    }

    public function homepageAction(){
        SessionAuth::requireLogIn();
        $sessId = ['id' => $_SESSION['isUserLogin']];
        $user = $this->userDAO->read($sessId)[0];

        $postsItem = $this->postDAO->getAllPosts();

        $postsItem = Iterator::likesIter($this->postDAO, $postsItem);

        $allPosts = [
            'user' => $user,
            'userPosts' => $postsItem
        ];


        $this->render('homepage', $allPosts);
    }

    public function searchAction(){
        SessionAuth::requireLogIn();
        $sessId = ['id' => $_SESSION['isUserLogin']];
        $user = $this->userDAO->read($sessId)[0];
        $this->render('search', ['user' => $user]);
    }

//    public function updateAction() {
//        SessionAuth::requireLogIn();
//
//    }
//
//    public function passwordAction() {
//        SessionAuth::requireLogIn();
//    }
}
