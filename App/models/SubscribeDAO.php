<?php

namespace App\Models;

use App\Core\App;

class SubscribeDAO
{
    public function follow($follower_id, $following_id){
        $data = [
            'follower' => $follower_id,
            'following' => $following_id
        ];
        $query = "INSERT INTO following_sys (follower_id, following_id) VALUES (:follower, :following)";
        App::getDB()->query($query, $data);
    }

    public function unfollow($follower_id, $following_id){
        $data = [
            'follower' => $follower_id,
            'following' => $following_id
        ];
        $query = "DELETE FROM following_sys WHERE follower_id = :follower AND following_id = :following";
        App::getDB()->query($query, $data);
    }


    public function isFollow($follower_id, $following_id){
        $data = [
            'follower' => $follower_id,
            'following' => $following_id
        ];
        $query = "SELECT * FROM following_sys WHERE follower_id = :follower AND following_id = :following";
        $result = App::getDB()->column($query, $data);
        return !empty($result);
    }

    public function getAllUserFollows($user_id, array $option){
//        $extraction = 'users.username, users.firstname, users.lastname, users.profileimg, users.background, users.id';
        $query = "SELECT * FROM users JOIN following_sys on users.id = following_sys.".$option[1]." WHERE ".$option[0]." = :".$option[0] ;
        return App::getDB()->column($query, [$option[0] => $user_id]);
    }

    public function findFollow($follower_id, $following_id){}

    public function getTotalFollows($user_id, array $option){}

    public function getTotalFollower($user_id){}

    public function getTotalFollowingByUserName($username){}

    public function getTotalFollowersByUserName($username){}

    public function getFollowingByUserName($username){}

    public function getFollow($id){}

    public function getFollowersByUserName($username){}

}
