<?php


namespace App\Models;


use App\Core\App;

class MessageDAO
{
    function create(array $data) {

        $part1 = " ";
        $part2 = " ";

        foreach ($data as $key => $value) {
            $part1 .= $key . ", ";
            $part2 .= ":" . $key . ", ";
        }

        $part1 = substr($part1, 0 , strlen($part1) - 2);
        $part2 = substr($part2, 0 , strlen($part2) - 2);

        $query = "INSERT INTO messages ( " . $part1 . " ) VALUES ( " . $part2 . " )";
        App::getDB()->query($query, $data);
    }

    function getMessages($me, $companion, $last_id = 0, $limit = false) {
        $data = [
            'me' => $me,
            'companion' => $companion,
        ];

        $limit = $limit ? ' DESC LIMIT 1': '';

        $query = "SELECT * FROM messages WHERE";
        $query .= " ((sender_id = :me AND recipient_id = :companion)";
        $query .= " OR (sender_id = :companion AND recipient_id = :me))";
        $query .= " AND (id > '$last_id') ORDER BY created_at" . $limit;
        return App::getDB()->column($query, $data);
    }

//    function getLastMessage($me, $companion) {
//        $query = "SELECT * FROM messages WHERE sender_id = :sender_id ORDER BY created_at DESC LIMIT 1";
//    }

    function getAllCompanions($me) {
        $query = "SELECT DISTINCT users.* FROM messages JOIN users ON messages.recipient_id = users.id";
        $query .= " WHERE (messages.recipient_id = :me OR messages.sender_id = :me) AND NOT users.id = :me";
//        $query = "SELECT DISTINCT users.*, FROM messages JOIN users on messages.recipient_id = users.id";
//        $query .= " WHERE (messages.recipient_id = :me OR messages.sender_id = :me) AND NOT users.id = :me";
//        $query .= " ORDER BY messages.created_at DESC";
        return App::getDB()->column($query, ['me' => $me]);
    }

    function read() {}

    function update($id, $body) {
        $data = [
            'id' => $id,
            'body' => $body
        ];
        $query = "UPDATE messages SET body = :body WHERE id = :id";
        App::getDB()->query($query, $data);
    }

    function delete($id) {
        $query = "DELETE FROM messages WHERE id = :id";
        App::getDB()->query($query, ['id' => $id]);
    }
}
