<?php

namespace App\Models;

use App\Core\App;
use App\Entity\User;

class UserDAO
{
    function create(User $user, $tbName = "users")
    {
        $dataUser["firstName"] = $user->getFirstName();
        $dataUser["lastName"] = $user->getLastName();
        $dataUser["email"] = $user->getEmail();
        $dataUser["userName"] = $user->getUserName();
        $dataUser["dob"] = $user->getDob();
        $dataUser["password"] = password_hash($user->getPassword(), PASSWORD_DEFAULT);

        $part1 = " ";
        $part2 = " ";

        foreach ($dataUser as $key => $value) {
            $part1 .= $key . ", ";
            $part2 .= ":" . $key . ", ";
        }

        $part1 = substr($part1, 0, strlen($part1) - 2);
        $part2 = substr($part2, 0, strlen($part2) - 2);

        $query = "INSERT INTO " . $tbName . "( " . $part1 . " ) VALUES ( " . $part2 . " )";
        App::getDB()->query($query, $dataUser);
    }

    function update($userFromDb, $tbName = 'users'){
        $toStringArr = " ";
        $argsArray = [];
        foreach ($userFromDb as $key => $value) {
            if ($key == "password") {
                $value = password_hash($value, PASSWORD_DEFAULT);
            }
            $argsArray[$key] = $value;
            $toStringArr .= $key . " = :" . $key . ", ";
        }

        $toStringArr = preg_replace('/, $/', "", $toStringArr);

        $query = "UPDATE " . $tbName . " SET " . $toStringArr . " WHERE id = " . $_SESSION['isUserLogin'] . ";";

        App::getDB()->query($query, $argsArray);
    }

    function read(array $selectArgs, $tbName = "users")
    {
        $toStringArr = '';
        foreach ($selectArgs as $key => $value) {
            $toStringArr .= " " . $key . '= :' . $key . ' AND ';
        }

        $toStringArr = preg_replace('/ AND $/', ";", $toStringArr);

        $query = "SELECT * FROM " . $tbName . " WHERE " . $toStringArr;

        return App::getDB()->column($query, $selectArgs);
    }

    function search(string $search) {
        $conditions = [
            "LOCATE('$search', username) > 0",
            "LOCATE('$search', firstname) > 0",
            "LOCATE('$search', lastname) > 0"
        ];
        $query = "SELECT * FROM users WHERE $conditions[0] OR $conditions[1] OR $conditions[2];";
        return App::getDB()->column($query);
    }

    function delete()
    {

    }
}
