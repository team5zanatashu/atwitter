<?php

namespace App\Models;

use App\Core\App;
use App\Entity\Post;

class PostDAO
{
    public function create($data, $tbName) {
        $part1 = " ";
        $part2 = " ";

        foreach ($data as $key => $value) {
            $part1 .= $key . ", ";
            $part2 .= ":" . $key . ", ";
        }

        $part1 = substr($part1, 0 , strlen($part1) - 2);
        $part2 = substr($part2, 0 , strlen($part2) - 2);

        $query = "INSERT INTO ". $tbName ."( " . $part1 . " ) VALUES ( " . $part2 . " )";
        App::getDB()->query($query, $data);
    }

    public function delete($selectArgs, $tbName){
        $toStringArr = '';
        foreach ($selectArgs as $key => $value) {
            $toStringArr .= " " . $key . '= :' . $key . ' AND ';
        }

        $toStringArr = preg_replace('/ AND $/', ";", $toStringArr);// SELECT * FROM users WHERE email = :email OR username = :username;

        $query = "DELETE FROM " . $tbName . " WHERE " . $toStringArr;

        return App::getDB()->query($query, $selectArgs);
    }

    public function postPost(Post $post){
        $post_data = [
            "user_id"       => $post->getUserId(),
            "body"          => $post->getBody(),
            "image"     => $post->getImage(),
        ];
        $this->create($post_data, 'posts');

    }

    public function getAllPosts($tbName = "posts"){
        $query = "SELECT posts.*, users.username, users.firstname, users.lastname, users.profileimg FROM posts JOIN users on posts.user_id = users.id ORDER BY posts.created_at DESC";
        return App::getDB()->column($query, []);
    }

    public function getAllPostsByUserId($user_id, $tbName = "posts"){
//        $query = "SELECT * FROM posts JOIN users on posts.user_id = users.id WHERE posts.user_id = :user_id ORDER BY posts.created_at DESC";
        $query = "SELECT * FROM " . $tbName . " WHERE user_id = :user_id ORDER BY created_at DESC; ";
        return App::getDB()->column($query, ["user_id" => $user_id]);
    }

    public function getAllPostsByUserName($username){}

    public function like($user_id, $post_id){
        $like_data = [
            'user_id' => $user_id,
            'post_id' => $post_id
        ];
        $this->create($like_data, 'likes');
    }

    public function unlike($user_id, $post_id){
        $like_data = [
            'user_id' => $user_id,
            'post_id' => $post_id
        ];
        $this->delete($like_data, 'likes');
    }

    public function isLike($user_id, $post_id){
        $query = "SELECT * FROM likes WHERE user_id = :user_id AND post_id = :post_id;";
        $result = App::getDB()->column($query, ["user_id" => $user_id, "post_id" => $post_id]);
        return empty($result) ? 'unliked' : 'liked';
    }

    public function getTotalLikes($post_id){
        $query = "SELECT * FROM likes WHERE post_id = :post_id;";
        $total = App::getDB()->column($query, ["post_id" => $post_id]);
        return count($total);
    }

    public function deletepost($user_id, $post_id){
        $delete_data = [
            'user_id' => $user_id,
            'id' => $post_id
        ];
        $this->delete($delete_data, 'posts');
    }
}
