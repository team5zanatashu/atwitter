<?php


namespace App\Helpers;


class Iterator
{
    public static function trimSpaces(array $input) {
        foreach ($input as $key => $data) {
            $input[$key] = trim($data);
        }
        return $input;
    }

    public static function userFilter(array $user, array $keys) {
        if (isset($user['profileimg'])) {
            $checkProfileImg = preg_match('/public\/img\//', $user['profileimg']);
            if(!$checkProfileImg) {
                $user['profileimg'] = LOCALHOST.'public/img/avatars/'.$user['profileimg'];
            }
        }
        if (isset($user['background'])) {
            $checkBackground = preg_match('/public\/img\//', $user['background']);
            if(!$checkBackground) {
                $user['background'] = LOCALHOST.'public/img/profile_backgrounds/'.$user['background'];
            }
        }
        $keys = array_flip($keys);
        return array_diff_key($user, $keys);
    }

    public static function usersIter($DAO_instance, array $users) {
        $iter = new \ArrayIterator($users);
        foreach($iter as $key => $item) {
            $followers = $DAO_instance->getAllUserFollows($item['id'], ['following_id', 'follower_id']);
            $users[$key]['followers'] = count($followers);

            $followings = $DAO_instance->getAllUserFollows($item['id'], ['follower_id', 'following_id']);
            $users[$key]['following'] = count($followings);
            $users[$key] = self::userFilter($users[$key],['id', 'email', 'dob', 'bio', 'password', 'created_at']);
        }
        return $users;
    }

    public static function subscribesIter($DAO_instance, array $subscribes, string $type) {
//        $isSubscribed = $type === 'following';
        $type .= '_id';
        $iter = new \ArrayIterator($subscribes);
        foreach($iter as $key => $item) {
            $followers = $DAO_instance->getAllUserFollows($item[$type], ['following_id', 'follower_id']);
            $subscribes[$key]['followers'] = count($followers);

            $followings = $DAO_instance->getAllUserFollows($item[$type], ['follower_id', 'following_id']);
            $subscribes[$key]['following'] = count($followings);
            $subscribes[$key] = self::userFilter($subscribes[$key],['id', 'email', 'dob', 'bio', 'password', 'created_at']);
            $subscribes[$key]['isMe'] = $item[$type] === $_SESSION['isUserLogin'];
            $subscribes[$key]['isMeSubscribed'] = $DAO_instance->isFollow($_SESSION['isUserLogin'], $item[$type]);
        }
        return $subscribes;
    }

    public static function likesIter ($DAO_instance, $posts_arr) {

        $iter = new \ArrayIterator($posts_arr);
        foreach ($iter as $key => $post) {
            $count_likes = $DAO_instance->getTotalLikes($post['id']);
            $is_liked = $DAO_instance->isLike($_SESSION['isUserLogin'], $post['id']);
            $posts_arr[$key] = self::userFilter($posts_arr[$key], []);
            $posts_arr[$key]['likes'] = $count_likes;
            $posts_arr[$key]['isLiked'] = $is_liked;
            if($posts_arr[$key]['image']) {
                $posts_arr[$key]['image'] = LOCALHOST.'public/img/posts_img/'.$posts_arr[$key]['image'];
            }
            if($posts_arr[$key]['user_id'] == $_SESSION['isUserLogin']){
                $posts_arr[$key]['canDel'] = true;
            }
            else {
                $posts_arr[$key]['canDel'] = false;
            }
        }
        return $posts_arr;
    }

    public static function postsIter ($posts_arr, $user) {
        $user = self::userFilter($user, ['dob', 'bio', 'background']);
        $iter = new \ArrayIterator($posts_arr);
        foreach ($iter as $key => $post) {
            $posts_arr[$key] = array_merge($user, $post);
//            if($posts_arr[$key]['image']) {
//                $posts_arr[$key]['image'] = LOCALHOST.'public/img/posts_img/'.$posts_arr[$key]['image'];
//            }
        }
        return $posts_arr;
    }

    public static function chatsIter($DAO_instance, $companions) {
        $iter = new \ArrayIterator($companions);
        foreach ($iter as $key => $companion) {
            $lastMes = $DAO_instance->getMessages($_SESSION['isUserLogin'], $companion['id'], 0, true)[0];
            $companion = self::userFilter($companion, ['id', 'email', 'dob', 'bio', 'password', 'created_at', 'background']);
            $companions[$key] = array_merge($lastMes, $companion);
            $companions[$key]['isMine'] = $_SESSION['isUserLogin'] === $lastMes['sender_id'];
        }
        return $companions;
    }

    public static function messagesIter($DAO_instance, $messages) {
        $iter = new \ArrayIterator($messages);
        foreach ($iter as $key => $message) {
            $user = $DAO_instance->read(['id' => $message['sender_id']])[0];
            $user = self::userFilter($user, ['id', 'email', 'dob', 'bio', 'password', 'created_at', 'background']);
            $messages[$key] = array_merge($user, $message);
            $messages[$key]['isMine'] = $_SESSION['isUserLogin'] === $message['sender_id'];
        }
        return $messages;
    }

}
