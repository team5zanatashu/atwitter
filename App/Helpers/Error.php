<?php


namespace App\Helpers;


class Error{
    function my_err($nam, $msg, $file, $line){
        die("$nam, $msg, $file, $line");
    }
    function exeptionHandler($e){
        die($e->getMessage()."<hr>".$e->getLine()."<hr>".$e->getFile()."<hr>");
    }
    function funcerrex(){
        //назначаем функцию обработчика ошибок
        set_error_handler([$this, "my_err"]);

        //назначаем функцию обработчика исключений
        set_exception_handler([$this, "exeptionHandler"]);
    }
}
