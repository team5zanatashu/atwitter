<?php


namespace App\Helpers;


class Validation
{
    private static $err_data = [];


    private static function checkFirstname(string $firstname){
        if(empty($firstname)) {
            self::$err_data['firstname_err'] = 'Field is empty. Please fill it.';
        }
        else{
            if(strlen($firstname) < 3){
                self::$err_data['firstname_err'] = 'Too short firstname. Firstname should have at least three characters.';
            }
            else if(strlen($firstname) > 30){
                self::$err_data['firstname_err'] = 'Too long firstname. Firstname shouln\'d have more than thirty characters.';
            }
            else if (!preg_match('/^[a-zA-Zа-яА-Я-]{3,29}[a-zA-Zа-яА-Я]$/',$firstname)) {
                self::$err_data['firstname_err'] = 'Invalid firstname. Firstname can\'t contain symbols (except "-"), numbers or spaces';
            }
        }
    }


    private static function checkLastname(string $lastname){
        if(empty($lastname)) {
            self::$err_data['lastname_err'] = 'Field is empty. Please fill it.';
        }
        else{
            if(strlen($lastname) < 3){
                self::$err_data['lastname_err'] = 'Too short lastname. Lastname should have at least three characters.';
            }
            else if(strlen($lastname) > 30){
                self::$err_data['lastname_err'] = 'Too long lastname. Lastname shouln\'d have more than thirty characters.';
            }
            else if (!preg_match('/^[a-zA-Zа-яА-Я-]{3,29}[a-zA-Zа-яА-Я]$/', $lastname)) {
                self::$err_data['lastname_err'] = 'Invalid lastname. Lastname can\'t contain symbols (except "-"), numbers or spaces';
            }
        }
    }


    private static function checkEmail(string $email){
        if(empty($email)) {
            self::$err_data['email_err'] = 'Field is empty. Please fill it.';
        }
        else{
             if (!preg_match('/^[a-zA-Z]+\d*[a-zA-Z]+\d*@[a-z]+\.[a-z]{2,3}$/', $email)) {
                self::$err_data['email_err'] = 'Invalid email. Email can\'t contain symbols (except "@") or spaces';
            }
        }
    }


    private static function checkUsername(string $username){
        if(empty($username)) {
            self::$err_data['username_err'] = 'Field is empty. Please fill it.';
        }
        else{
            if(strlen($username) < 3){
                self::$err_data['username_err'] = 'Too short username. Username should have at least three characters.';
            }
            else if(strlen($username) > 30){
                self::$err_data['username_err'] = 'Too long username. Username shouln\'d have more than fifteen characters.';
            }
            else if (!preg_match('/^(\w{3,15})$/', $username)) {
                self::$err_data['username_err'] = 'Invalid username. Username can\'t contain symbols (except "_"), numbers or spaces';
            }
        }
    }

    private static function checkDateOfBirth(string $dob) {
        if(strlen($dob) > 10) {
            self::$err_data['dob_err'] = 'Invalid date of birth. Please, check it and enter your date of birth.';
        }
        if (!preg_match('/(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.(19\d\d|20[01]\d|2020)/', $dob)) {
            self::$err_data['dob_err'] = 'Invalid date of birth. Please, check it and enter your date of birth.';
        }
    }

    private static function checkPassword(string $password){
        if(empty($password)) {
            self::$err_data['password_err'] = 'Field is empty. Please fill it.';
        }
        else{
            if(strlen($password) < 8){
                self::$err_data['password_err'] = 'Too short password. Password should have at least eight characters.';
            }
            else if (!preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z]{8,}$/', $password)) {
                self::$err_data['password_err'] = 'Invalid password. Password must contain one uppercase and number';
            }
        }
    }

    private static function checkConfirm(string $pass, string $confirm) {
        if ($pass !== $confirm) {
            self::$err_data['password_confirm_err'] = 'Password does not match.';
        }
    }

    private static function checkBio(string $bio) {
        if(strlen($bio) > 1000) {
            self::$err_data['bio_err'] = "To long information. Make it less then 1000 symbols";
        }
        else if (preg_match('/^\s+.+\s+$/', $bio)) {
            self::$err_data['bio_err'] = "Some spaces around the edges.";
        }
    }

    private static function checkPost(string $post) {
        if(empty($post)) {
            self::$err_data['post_field_err'] = 'Field is empty. Please fill it.';
        }
        else{
            if(strlen($post) > 1000) {
                self::$err_data['post_field_err'] = 'Too long post text.';
            }
        }
    }


    public static function validationForUpdate(array $data) {
        if ($data['firstname']) {
            self::checkFirstname($data['firstname']);
        }
        if ($data['lastname']) {
            self::checkLastname($data['lastname']);
        }
        if ($data['email']) {
            self::checkEmail($data['email']);
        }
        if ($data['username']) {
            self::checkUsername($data['username']);
        }
        if ($data['bio']) {
            self::checkBio($data['bio']);
        }
        if ($data['dob'] !== "day.month.year" && $data['dob']) {
            self::checkDateOfBirth($data['dob']);
        }
        if (isset($data['password']) && $data['password']) {
            self::checkPassword($data['password']);
        }
        if (isset($data['confirm']) && $data['confirm']) {
            self::checkConfirm($data['password'], $data['confirm']);
        }

        return self::$err_data;
    }

    public static function validationForSignUp(array $data) {

        self::checkFirstname($data['firstname']);
        self::checkLastname($data['lastname']);
        self::checkEmail($data['email']);
        self::checkUsername($data['username']);
        self::checkDateOfBirth($data['dob']);
        self::checkPassword($data['password']);

        return self::$err_data;
    }

    public static function validationForLogin(array $data) {
        self::checkEmail($data['email']);
        self::checkPassword($data['password']);

        return self::$err_data;
    }

    public static function validationForPost(array $data) {
        self::checkPost($data['post_field']);

        return self::$err_data;
    }

}
