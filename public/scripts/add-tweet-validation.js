const postText = document.querySelectorAll('#input-post');
const postNumOfSymb = document.querySelector('#snum-post');
const postAddBtn = document.querySelector('#add-post');
const postForm = document.querySelectorAll('.card-add-post__form');
const postsList = document.querySelectorAll('.section-account__posts-list');

const requestUrl = window.location.origin + "/post/create";

    // //console.log(postText);

    // const body = {
    //     name: 'Nik',
    //     age: 23
    // };



function numberOfSymbols(len) {
    const num = postNumOfSymb.textContent;
    const reg = /\d+(?=\/)/;
    return num.replace(reg, String(len));
} 

function checkInputLen (len) {
    if (len > 1000) {
        postAddBtn.setAttribute("disabled", true);
    }
    else {
        if (postAddBtn.disabled) {
            postAddBtn.removeAttribute("disabled");
        }    
    }
}

function sendRequestFunc(method, url, body) {
    // debugger
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.responseType = 'json';
        // xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onload = function() {
            if(xhr.status >= 400) {
                reject(xhr.response);
            } else {
                resolve(xhr.response)
            }
        }
        xhr.onerror = function() {
            //console.log(xhr.response);
        }
        // xhr.send(JSON.stringify(body));
        xhr.send(body);
    });

}



postAddBtn.addEventListener('click', e => {
    // let postBody = postText[0].value;
    let postBody = new FormData(postForm[0]);
    sendRequestFunc('POST', requestUrl, postBody)
        .then(data => addPostFunc(data))// вернулась строка html кода после запроса
        .catch(err => console.log(err));
});


function addPostFunc(data) {
    // //console.log(data);
    let regExp = /(<li[^>]*>)((\n.*)*)(<\/li>)/i;
    // let regMatch = regExp.exec(data);
    let regExpResult = data.replace(regExp, '$2');
    // //console.log(regExpResult);
    let postLi = document.createElement('li');
    postLi.className = 'section-account__card section-account__card--custom';
    postLi.innerHTML = regExpResult;
    postsList[0].prepend(postLi);
    postText[0].value = '';
    postNumOfSymb.textContent = '0/1000';
    let lucas = document.querySelector('.like-icon');

    lucas.addEventListener('click', likeExec);
}









//+26 42 68 94
postText.forEach(el => {

    el.addEventListener('keydown', e => {
        if (e.keyCode == 8) {
            //console.log(window.location);
            //console.log(window.location.origin);
            const inpLength = e.target.value.length;
            postNumOfSymb.textContent = numberOfSymbols(inpLength);
            checkInputLen(inpLength);   
        }
    });

    el.addEventListener('keyup', e => {
        //console.log(el.offsetHeight);
        const inpLength = e.target.value.length;
        postNumOfSymb.textContent = numberOfSymbols(inpLength);
        checkInputLen(inpLength);
    });

    let isWrapped = false;

    el.addEventListener('scroll', e => {
        // let rowsNum = e.target.rows;
        if (e.target.rows > 1 && isWrapped) {
            isWrapped = false;
        }
        else {
            e.target.setAttribute("rows", `${++e.target.rows}`);
            isWrapped = true;
        }
    });

    el.addEventListener("blur", () => {
        const re = /^\s*$/gm
        if (re.test(el.value)) {
          el.rows = 1;
          el.value = "";
        }
    });
    
});


// function addRow (len) {
//     let rowLen = 82
//     let n = len / 82;
//     if 
// }
