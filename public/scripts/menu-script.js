/* eslint-disable no-restricted-globals */
/* eslint-disable prefer-const */
/* eslint-disable no-alert */
/* eslint-disable consistent-return */
/* eslint-disable eqeqeq */
/* eslint-disable no-plusplus */
/* eslint-disable prettier/prettier */

let email = document.querySelectorAll(".form-input_email");
let password = document.querySelectorAll(".form-input_psw");
let btn = document.querySelectorAll(".form-button");
const regex = RegExp(
  "^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$"
);
const regex1 = RegExp("^\\s*$");

function checkDataBase(email, password) {
  return false;
}

function requireFields() {
  if (!regex.test(email[0].value)) {
    email[0].style = "border-bottom: 1px solid red;";
  }
  if (regex1.test(password[0].value)) {
    password[0].style = "border-bottom: 1px solid red;";
  }
  if (regex.test(email[0].value)) {
    email[0].style = "border-bottom: 1px solid green;";
  }
  if (!regex1.test(password[0].value)) {
    password[0].style = "border-bottom: 1px solid green;";
  }
  if (regex.test(email[0].value) && !regex1.test(password[0].value)) {
    //location.href = "./user-profile.html";
  } else if (checkDataBase(email[0].value, password[0].value)) {
    ///тут проверяет есть ли в базе данных этот пользователь
    alert("Такого юзера не существует");
  }
}

email.forEach(el =>
  el.addEventListener("keydown", e => {
    if (e.keyCode == 13) {
      password[0].focus();
    }
  })
);

password.forEach(el =>
  el.addEventListener("keydown", e => {
    if (e.keyCode == 13) {
      requireFields();
    }
  })
);

btn.forEach(el => el.addEventListener("click", requireFields));
