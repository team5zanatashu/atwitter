// let form = document.querySelector(".form-reg");
// let fName = document.querySelector(".form-reg .firstname");
// let lName = form.querySelector(".lastname");
// let email = form.querySelector(".email");
// let username = form.querySelector(".username");
// let dateberth = form.querySelector(".dateberth");
let password = document.querySelector(".password");
let confirmpassword = document.querySelector(".confirmpassword");
// let btn = form.querySelector(".submit");




let fName = document.querySelectorAll(".form-reg .firstname");


fName.forEach(elem=>{
    elem.addEventListener("change", function(e) {
        let a = e.target.value.toString();
        if (!regExpName(a)) {
            e.target.style = "border-bottom: 1px solid red";
        } else if (regExpName(a)) {
            e.target.style = "border-bottom: 1px solid silver";
            e.target.innerHTML = "";
        }
    });
})

let lName = document.querySelectorAll(".form-reg .lastname");
lName.forEach(elem=>{

    elem.addEventListener("change", function(e) {
        let a = e.target.value.toString();
        if (!regExpName(a)) {
            e.target.style = "border-bottom: 1px solid red";
        } else if (regExpName(a)) {
            e.target.style = "border-bottom: 1px solid silver";
            e.target.innerHTML = "";
        }
    });
})

let email = document.querySelectorAll(".form-reg .email");

email.forEach(elem=>{
    elem.addEventListener("change", function(e) {
        let a = e.target.value.toString();
        if (!regExpEmail(a)) {
            e.target.style = "border-bottom: 1px solid red";
        } else if (regExpEmail(a)) {
            e.target.style = "border-bottom: 1px solid silver";
            e.target.innerHTML = "";
        }
    });
})


let username = document.querySelectorAll(".form-reg .username");

username.forEach(elem=>{

    elem.addEventListener("change", function(e) {
        let a = e.target.value.toString();
        if (!regExpEmail(a)) {
            e.target.style = "border-bottom: 1px solid red";
        } else if (regExpUser(a)) {
            e.target.style = "border-bottom: 1px solid silver";
            e.target.innerHTML = "";
        }
    });
})



let dateberth = document.querySelectorAll(".form-reg .dateberth");

dateberth.forEach(elem=>{

    elem.addEventListener("change", function(e) {
        let a = e.target.value.toString();
        if (!regExpData(a)) {
            e.target.style = "border-bottom: 1px solid red";
        } else if (regExpData(a)) {
            e.target.style = "border-bottom: 1px solid silver";
            e.target.innerHTML = "";
        }
    });

})


let btn = document.querySelectorAll(".form-reg .submit");
btn.forEach(elem=>{
    elem.addEventListener("click", function(e) {
    validate(e);
});

})

function validate(e) {
    if(fName[0].value == '')
    {
        event.preventDefault();
        fName[0].style ="border-bottom: 1px solid red";

    }
    if(lName[0].value == '')
    {
        event.preventDefault();
        lName[0].style ="border-bottom: 1px solid red";

    }
    if(email[0].value == '')
    {
        event.preventDefault();
        email[0].style ="border-bottom: 1px solid red";

    }
    if(username[0].value == '')
    {
        event.preventDefault();
        username[0].style ="border-bottom: 1px solid red";

    }
    if( dateberth[0].value == '')
    {
        event.preventDefault();
        dateberth[0].style ="border-bottom: 1px solid red";

    }
    if (
        password.value != confirmpassword.value |
        password.value == '' |
        confirmpassword.value == ''
    ) {
        event.preventDefault();
        password.style = " border-bottom: 1px solid red";
        confirmpassword.style = "border-bottom: 1px solid red";

        return false;
    }
}

// btn.addEventListener("click", function(e) {
//     validate(e);
// });

//FN LN
function regExpName(st) {
    let shablon = /^([a-zA-zа-яА-я]{3,30})$/;
    let rez = shablon.test(st);
    return rez;
}

//User Name
function regExpUser(st) {
    let shablon = /^([a-zA-z]{3,15})$/;
    return shablon.test(st);
}

// email
function regExpEmail(st) {
    let shablon = /^[a-zA-z]+\d*[a-zA-z]+\d*@[a-z]+\.[a-z]{2,5}$/;

    return shablon.test(st);
}

//data
function regExpData(st) {
    let shablon = /((0[1-9]|10|11|12)\.([0-2][0-9]|31|30)\.(1[0-9][0-9][0-9]|201[0-9]|2020))/g;
    let rez = shablon.test(st);

    return rez;
}