/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-expressions */
const tabs = document.querySelectorAll(".tweet-tabs");
const tabsContent = document.querySelectorAll(".content-tab");
let tabName = "tab1";

function checkTabName() {
  tabs.forEach(item => {
    item.classList.remove("tab-is-active");
  });
  this.classList.add("tab-is-active");
  tabName = this.getAttribute("data-tab-name");
  selectTabContent(tabName);
}

function selectTabContent(tabName) {
  tabsContent.forEach(content => {
    content.classList.contains(tabName)
      ? content.classList.add("tab-is-active")
      : content.classList.remove("tab-is-active");
  });
}

tabs.forEach(tab => {
  tab.addEventListener("click", checkTabName);
});
