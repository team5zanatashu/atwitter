/* eslint-disable consistent-return */
/* eslint-disable eqeqeq */
/* eslint-disable no-plusplus */
/* eslint-disable prettier/prettier */
const like = document.querySelectorAll(".like-icon");
const rt = document.querySelectorAll(".rt-icon");
const fav = document.querySelectorAll(".fav-icon");
const more = document.querySelectorAll(".more-icon");
const posts = document.querySelectorAll(".section-account__post-footer-actions");

let postActions = [];

function IsActive() {
  this.like = false,
  this.rt = false,
  this.fav = false,
  this.more = false
};

if (posts) {
  for(let i = 0; i < posts.length; i++) {
    let isActive = new IsActive()
    postActions.push(isActive)
  }
}

function actionIncrement(action) {
  if (action == undefined) {
    return;
  }
  let number = +action.textContent;
  return String(++number);
}

function actionDecrement(action) {
  if (action == undefined) {
    return;
  }
  let number = +action.textContent;
  return String(--number);
}

function actionChange(action, et, isActive) {
  let target;
  if (et.className == `mr-2 actions__${action}`) {
    target = et;
  } else if (et.parentNode.className == `mr-2 actions__${action}`) {
    target = et.parentNode;
  }
  let filterChange;
  switch (action) {
    case "like":
      filterChange = `filter: hue-rotate(30deg) saturate(20) contrast(100%);`;
      break;
    case "rt":
      filterChange = `filter: hue-rotate(230deg) saturate(20) contrast(100%);`;
      break;
    case "fav":
      filterChange = `filter: hue-rotate(200deg) saturate(20) contrast(110%);`;
      break;
    case "more":
      filterChange = `filter: hue-rotate(0deg) saturate(20) contrast(100%);`;
      break;
  }
  if (isActive[action] == false) {
    isActive[action] = true;
    target.style.cssText = filterChange;
    if (target.children[1] !== undefined) {
      target.children[1].textContent = actionIncrement(target.children[1]);
    }
  } else {
    isActive[action] = false;
    target.style.cssText = ``;
    if (target.children[1] !== undefined) {
      target.children[1].textContent = actionDecrement(target.children[1]);
    }
  }
}

function choosePost (action, event) {
  let parent;
  let tFooter;
  if(event.target.parentNode.className == `mr-2 actions__${action}`) {
    parent = event.target.parentNode.parentNode
  }
  else {
    parent = event.target.parentNode
  }
  for(let i = 0; i < posts.length; i++) {
    if(posts[i] == parent) {
      tFooter = postActions[i];
    }
  }
  return tFooter
}

function likeChange(event) {
  console.log(window.location);
  console.log(window.location.origin + '/post/like');
  let tFooter = choosePost("like", event)
  actionChange("like", event.target, tFooter);
}
for (let i = 0; i < like.length; i++) {
  like[i].addEventListener("click", likeChange);
}

function rtChange(event) {
  let tFooter = choosePost("rt", event)
  actionChange("rt", event.target, tFooter);
}
for (let i = 0; i < rt.length; i++) {
  rt[i].addEventListener("click", rtChange);
}

function favChange(event) {
  let tFooter = choosePost("fav",event)
  actionChange("fav", event.target, tFooter);
}
for (let i = 0; i < fav.length; i++) {
  fav[i].addEventListener("click", favChange);
}

function moreChange(event) {
  let tFooter = choosePost("more", event)
  actionChange("more", event.target, tFooter);
}
for (let i = 0; i < more.length; i++) {
  more[i].addEventListener("click", moreChange);
}
