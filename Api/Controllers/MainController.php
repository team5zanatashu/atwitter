<?php

namespace Api\Controllers;

use Api\Core\Controller;

class MainController extends Controller {

    public function indexAction(){
        $this->render('devcommunication');
    }

}
