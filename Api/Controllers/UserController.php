<?php

namespace Api\Controllers;

use App\Helpers\Iterator;
use App\Models\PostDAO;
use App\Models\SubscribeDAO;
use App\Models\UserDAO;
use Api\Core\Controller;
use App\Entity\User;
use App\Helpers\Validation;
use Api\Service\SessionAuth;


class UserController extends Controller {

    private $userDAO;
    private $postDAO;
    private $subscribeDAO;

    public function __construct() {
        $this->userDAO = new UserDAO();
        $this->postDAO = new PostDAO();
        $this->subscribeDAO = new SubscribeDAO();
    }


    public function loginAction() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $_POST = Iterator::trimSpaces($_POST);
        $errors = Validation::validationForLogin($_POST);
        if(empty($errors)) {
            $user = new User();
            $user->setForLogin($_POST);
            $userFromDb = $this->userDAO->read(['email' => $user->getEmail()]);
            if(!empty($userFromDb)) {
                $userFromDb = $userFromDb[0];
                $user_id = $userFromDb['id'];
                $username = $userFromDb['username'];
                $checkHash = password_verify($user->getPassword(), $userFromDb['password']);
            }
            if(isset($user_id, $checkHash)) {
                SessionAuth::logIn($user_id);
                $this->responseAPI(200, 'ok', $username);
            }
            else {
                $this->responseAPI(404, 'not found', 'User not found!');
            }
        }
        else {
            $params = [
                'errors' => $errors,
                'formdata' => $_POST
            ];
            $this->responseAPI(400, 'invalid', $params);
        }
    }

    public function logoutAction() {
        $username = $this->userDAO->read(['id' => $_SESSION['isUserLogin']])[0]['username'];
        SessionAuth::logOut();
        if(empty($_SESSION)) {
            $this->responseAPI(200, 'ok', $username);
        }
        else {
            $this->responseAPI(500, 'failed', 'Internal Server Error');
        }

    }

    public function signupAction(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        $_POST['dob'] = $_POST['day']. '.' .$_POST['month']. '.' .$_POST['year'];

        $_POST = Iterator::trimSpaces($_POST);
        $errors = Validation::validationForSignUp($_POST);
        if (empty($errors)) {
            $user = new User();
            $user->setForSignUp($_POST);
            $checkEmail = $this->userDAO->read(['email' => $user->getEmail()]);
            $checkUserName = $this->userDAO->read(['username' => $user->getUserName()]);

            if (!$checkEmail && !$checkUserName) {
                $this->userDAO->create($user);
                SessionAuth::logIn($this->userDAO->read(['email' => $user->getEmail()])[0]['id']);
                $this->responseAPI(200, 'ok', $user->getUserName());
            }
            else {
                $params = [
                    'error' => 'Such user already exists',
                    'email' => (boolean)$checkEmail,
                    'username' => (boolean)$checkUserName
                ];
                $this->responseAPI(400, 'exists', $params);
            }
        }
        else {
            $params = [
                'errors' => $errors,
                'formdata' => $_POST
            ];
            $this->responseAPI(400, 'invalid', $params);
        }
    }


    public function accountAction(string $username = ''){
        SessionAuth::requireLogIn($this);
//        $defaultUser = $this->userDAO->read(['id' => $_SESSION['isUserLogin']])[0]['username'];
        if($_SERVER['REQUEST_METHOD'] == "GET") {

            $user = $this->userDAO->read(['username' => $username])[0];
            if(empty($user)) {
//                header('Location: '.LOCALHOST.'user/account/'. $defaultUser);
                $this->responseAPI(404, 'not found', 'No such user on DevCommunication');
                exit;
            }
            $userPosts = $this->postDAO->getAllPostsByUserId($user['id']);
            $user['isMe'] = $user['id'] === $_SESSION['isUserLogin'];
            if (!$user['isMe']) {
                $user['isMeSubscribed'] = $this->subscribeDAO->isFollow($_SESSION['isUserLogin'], $user['id']);
            }
            $user = Iterator::userFilter($user, ['id', 'email', 'password', 'created_at']);
            $userPosts = Iterator::postsIter($userPosts, $user);
            $userPosts = Iterator::likesIter($this->postDAO, $userPosts);

            $params = [
              'user' => $user,
              'userPosts' => $userPosts
            ];
            $this->responseAPI(200, 'ok', $params);
        }
    }

    public function editAction(){
        SessionAuth::requireLogIn($this);
        $userDefault = $this->userDAO->read(["id" => $_SESSION['isUserLogin']])[0];
        $userFiltered = Iterator::userFilter($userDefault, ['id', 'password', 'created_at']);
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['day'])) {
                $_POST['dob'] = $_POST['day']. '.' .$_POST['month']. '.' .$_POST['year'];
            }
            $_POST = Iterator::trimSpaces($_POST);
            $errors = Validation::validationForUpdate($_POST);
            if (isset($_POST['password']) && !password_verify($_POST['current-password'], $userDefault['password'])) {
                $errors["current_password"] =  "Invalid Password";
                //здесь можно добавить счетчик на неправильно введенный пароль и редиректить на начальную с логаутом
            }
            if (empty($errors)) {
                $tempFields = [];
                if (isset($_FILES['profileimg']) && $_FILES['profileimg']["name"]) {
                    $newdir = dirname(__DIR__, 2).'/public/img/avatars/'.$_SESSION['isUserLogin'].$_FILES["profileimg"]["name"];
                    move_uploaded_file($_FILES["profileimg"]["tmp_name"], $newdir);
                    $tempFields['profileimg'] = $_SESSION['isUserLogin'].$_FILES['profileimg']["name"];
                }
                if (isset($_FILES['background']) && $_FILES['background']["name"]) {
                    $newdir = dirname(__DIR__, 2).'/public/img/profile_backgrounds/'.$_SESSION['isUserLogin'].$_FILES["background"]["name"];
                    move_uploaded_file($_FILES["background"]["tmp_name"], $newdir);
                    $tempFields['background'] = $_SESSION['isUserLogin'].$_FILES['background']["name"];
                }
                foreach ($_POST as $key => $value) {
                    if (array_key_exists($key, $userDefault) && !preg_match('/^\s*$/', $value) &&
                        $value !== "day.month.year"){
                        $tempFields[$key] = $_POST[$key];
                    }
                }
                $checkEmail = $this->userDAO->read(['email' => $_POST['email']]);
                $checkUserName = $this->userDAO->read(['username' => $_POST['username']]);

                if (!$checkEmail && !$checkUserName) {
                    $this->userDAO->update($tempFields);
                    $userDefault = $this->userDAO->read(["id" => $_SESSION['isUserLogin']])[0];
                    $userFiltered = Iterator::userFilter($userDefault, ['id', 'password', 'created_at']);
                    $this->responseAPI(200, 'ok', $userFiltered);
                }
                else {
                    $this->responseAPI(400, 'exists', 'Such user already exists');
                }
            }
            else {
                $params = [
                    'user' => $userFiltered,
                    'errors' => $errors,
                    'formdata' => $_POST
                ];
                $this->responseAPI(400, 'invalid', $params);
            }
        }
        else {
            $this->responseAPI(200, 'ok', $userFiltered);
        }
    }

    public function homepageAction(){
        SessionAuth::requireLogIn($this);
        $sessId = ['id' => $_SESSION['isUserLogin']];
        $user = $this->userDAO->read($sessId)[0];
//        $user = Iterator::userFilter($user, ['id']);
        $postsItem = $this->postDAO->getAllPosts();

        $user = Iterator::userFilter($user, ['id', 'email', 'password', 'created_at']);
//        $userPosts = Iterator::postsIter($postsItem, $user);
        $postsItem = Iterator::likesIter($this->postDAO, $postsItem);

        $allPosts = [
            'user' => $user,
            'userPosts' => $postsItem
        ];

        $this->responseAPI(200, 'ok', $allPosts);
//        $this->render('homepage', $allPosts);
    }

    public function searchAction(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        $result = $this->userDAO->search($_POST['query']);
        $result = Iterator::usersIter($this->subscribeDAO, $result);
        $params = [
            'result' => $result,
            'count' => count($result)
        ];
        $this->responseAPI(200, 'ok', $params);
    }

//    public function updateAction() {
//        SessionAuth::requireLogIn();
//
//    }
//
//    public function passwordAction() {
//        SessionAuth::requireLogIn();
//    }
}
