<?php


namespace Api\Controllers;


use Api\Core\Controller;
use Api\Service\SessionAuth;
use App\Helpers\Iterator;
use App\Models\MessageDAO;
use App\Models\UserDAO;

class MessageController extends Controller
{
    private $messageDAO;
    private $userDAO;

    public function __construct()
    {
        $this->messageDAO = new MessageDAO();
        $this->userDAO = new UserDAO();
    }

    public function allchatsAction() {
        if(SessionAuth::isLoggedIn()) {
//            $_POST = json_decode(file_get_contents('php://input'), true);
            $user = $_SESSION['isUserLogin'];
            $chats = $this->messageDAO->getAllCompanions($user);
            $chats = Iterator::chatsIter($this->messageDAO, $chats);
            $this->responseAPI(200, 'ok', $chats);
        }
        else {
            $this->responseAPI(401, 'unauthorized', 'Login or Register to see this page');
        }
    }

    public function singlechatAction() {
        if(SessionAuth::isLoggedIn()) {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $companion = $this->userDAO->read(['username' => $_POST['username']])[0]['id'];
            $messages = $this->messageDAO->getMessages($_SESSION['isUserLogin'], $companion);
            if (!empty($messages[0])) {
                $messages = Iterator::messagesIter($this->userDAO, $messages);
                $this->responseAPI(200, 'ok', $messages);
            }
            else {
                $this->responseAPI(500, 'failed', 'Internal Server Error');
            }
        }
        else {
            $this->responseAPI(401, 'unauthorized', 'Login or Register to see this page');
        }
    }

    public function updatechatAction() {
        if(SessionAuth::isLoggedIn()) {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $companion = $this->userDAO->read(['username' => $_POST['username']])[0]['id'];
            $lastId = (int)$_POST['id'];
            $messages = $this->messageDAO->getMessages($_SESSION['isUserLogin'], $companion, $lastId);
            if (!empty($messages[0])) {
                $messages = Iterator::messagesIter($this->userDAO, $messages);
                $this->responseAPI(200, 'ok', $messages);
            }
            else {
                $this->responseAPI(200, 'ok', false);
            }
        }
        else {
            $this->responseAPI(401, 'unauthorized', 'Login or Register to see this page');
        }
    }

    public function sendAction() {
        if(SessionAuth::isLoggedIn()) {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $data = [
                'sender_id' => $_SESSION['isUserLogin'],
                'recipient_id' => $this->userDAO->read(['username' => $_POST['username']])[0]['id'],
                'body' => $_POST['body']
            ];
            $this->messageDAO->create($data);
            $message = $this->messageDAO->getMessages($data['sender_id'], $data['recipient_id'], 0, true);
            if (!empty($message[0])) {
                $message = Iterator::messagesIter($this->userDAO, $message);
                $this->responseAPI(200, 'ok', $message[0]);
            }
            else {
                $this->responseAPI(500, 'failed', 'Internal Server Error');
            }
        }
        else {
            $this->responseAPI(401, 'unauthorized', 'Login or Register to see this page');
        }
    }

    public function editAction() {
        if(SessionAuth::isLoggedIn()) {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $this->messageDAO->update($_POST['id'], $_POST['body']);
            $id = (int)$_POST['id'] - 1;
            $this->read($id, $_POST['username']);
        }
        else {
            $this->responseAPI(401, 'unauthorized', 'Login or Register to see this page');
        }
    }

    public function deleteAction() {
        if(SessionAuth::isLoggedIn()) {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $this->messageDAO->delete($_POST['id']);
            $id = (int)$_POST['id'] - 1;
            $this->read($id, $_POST['username']);
        }
        else {
            $this->responseAPI(401, 'unauthorized', 'Login or Register to see this page');
        }
    }

    private function read($id, $companion) {
        $companion = $this->userDAO->read(['username' => $companion])[0]['id'];
        $messages = $this->messageDAO->getMessages($_SESSION['isUserLogin'], $companion, $id);
        if (!empty($messages[0])) {
            $messages = Iterator::messagesIter($this->userDAO, $messages);
            $this->responseAPI(200, 'ok', $messages);
        }
        else {
            $this->responseAPI(200, 'ok', false);
        }
    }
}
