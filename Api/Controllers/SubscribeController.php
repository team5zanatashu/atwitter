<?php


namespace Api\Controllers;


use Api\Core\Controller;
use Api\Service\SessionAuth;
use App\Helpers\Iterator;
use App\Models\SubscribeDAO;
use App\Models\UserDAO;

class SubscribeController extends Controller
{
    private $userDAO;
    private $subscribeDAO;

    public function __construct() {
        $this->userDAO = new UserDAO();
        $this->subscribeDAO = new SubscribeDAO();
    }

    public function getSubscribeStatus($following) {
        $follower = $_SESSION['isUserLogin'];
//        $determine = $this->subscribeDAO->isFollow($follower, $following);
        return $this->subscribeDAO->isFollow($follower, $following);
    }

    public function changeAction() {
        if (SessionAuth::isLoggedIn()) {
            $follower = $_SESSION['isUserLogin'];
            $following = json_decode(file_get_contents('php://input'), true)['username'];
            $following = $this->userDAO->read(['username' => $following])[0]['id'];
            if ($this->getSubscribeStatus($following)) {
                $this->subscribeDAO->unfollow($follower, $following);
                $this->responseAPI(200, 'ok', 'unsubscribed');
            }
            else {
                $this->subscribeDAO->follow($follower, $following);
                $this->responseAPI(200, 'ok', 'subscribed');
            }
        }
        else {
            $this->responseAPI(401, 'unauthorized', 'can not subscribe');
        }
    }

    public function followersAction() {
        $username = json_decode(file_get_contents('php://input'), true)['username'];
        $user_id = $this->userDAO->read(['username' => $username])[0]['id'];
        $followers = $this->subscribeDAO->getAllUserFollows($user_id, ['following_id', 'follower_id']);
        $followers = Iterator::subscribesIter($this->subscribeDAO, $followers, 'follower');
        $this->responseAPI(200, 'ok', $followers);
    }

    public function followingAction() {
        $username = json_decode(file_get_contents('php://input'), true)['username'];
        $user_id = $this->userDAO->read(['username' => $username])[0]['id'];
        $followings = $this->subscribeDAO->getAllUserFollows($user_id, ['follower_id', 'following_id']);
        $followings = Iterator::subscribesIter($this->subscribeDAO, $followings, 'following');
        $this->responseAPI(200, 'ok', $followings);
    }

    public function friendsAction() {
        $username = json_decode(file_get_contents('php://input'), true)['username'];
        $user_id = $this->userDAO->read(['username' => $username])[0]['id'];
        $followers = $this->subscribeDAO->getAllUserFollows($user_id, ['following_id', 'follower_id']);
        $followings = $this->subscribeDAO->getAllUserFollows($user_id, ['follower_id', 'following_id']);
        $followers = Iterator::usersIter($this->subscribeDAO, $followers);
        $followings = Iterator::usersIter($this->subscribeDAO, $followings);
        $friends = [
            'followers' => $followers,
            'followings' => $followings
        ];
        $this->responseAPI(200, 'ok', $friends);
    }
}
