<?php


namespace Api\Controllers;


use Api\Core\Controller;

class ErrorsController extends Controller {

    public function err404Action(){
//        setcookie('preURL', $_SERVER['HTTP_REFERER'], time() + 180, '/');
        $this->render('errors/404');
    }

    public function comebackAction() {
        $arr = require dirname(__DIR__).'/config/routes.php';
        $preURL = $_SERVER['HTTP_REFERER'];
        foreach ($arr as $route => $value) {
            $reg = '~'.LOCALHOST.$route.'~';
            if (preg_match($reg, $preURL, $matches)) {
                $back = $matches;
            }
        }
        header('Location: '.$back[0]);
//        setcookie('preURL', '', time() - 3600, '/');
    }

}
