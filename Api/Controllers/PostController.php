<?php


namespace Api\Controllers;

use Api\Core\Controller;
use App\Entity\Post;
use App\Entity\User;
use App\Helpers\Iterator;
use App\Helpers\Validation;
use App\Models\PostDAO;
use App\Models\UserDAO;
use Api\Service\SessionAuth;

class PostController extends Controller
{
    private $postDAO;
    private $userDAO;

    public function __construct() {
        $this->postDAO = new PostDAO();
        $this->userDAO = new UserDAO();
    }

    public function createAction() {
        SessionAuth::isLoggedIn();
//        $postText = json_decode(file_get_contents('php://input'), true);
//        $_POST['post_field'] = $postText;
        $newdir = dirname(__DIR__, 2).'/public/img/posts_img/'.$_FILES["image"]["name"];
        move_uploaded_file($_FILES["image"]["tmp_name"], $newdir);

        $_POST = Iterator::trimSpaces($_POST);
        $errors = Validation::validationForPost($_POST);
        if(empty($errors)) {
            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                $userId = $_SESSION['isUserLogin'];
                $postData = [
                    "post_field" => $_POST['post_field'],
                    "post_image" => $_FILES["image"]["name"],
                    "post_user" => $userId
                ];
                $post = new Post();
                $post->setPost($postData);
                $this->postDAO->postPost($post);

                $userPosts[0] = $this->postDAO->getAllPostsByUserId($_SESSION['isUserLogin'])[0];
                $user = $this->userDAO->read(['id' => $_SESSION['isUserLogin']])[0];
                if(!empty($user)) {
                    $user = Iterator::userFilter($user,['id', 'email', 'password', 'created_at', 'dob', 'bio']);
                    $userPosts = Iterator::postsIter($userPosts, $user);
                    $userPosts = Iterator::likesIter($this->postDAO, $userPosts);
                    $params = [
                        'user' => $user,
                        'userPosts' => $userPosts
                    ];
                    $this->responseAPI(200, 'ok', $params);
                }
                else {
                    $this->responseAPI(404, 'not found', 'user not found');
                }
            }
        } else {
            $this->responseAPI(400, 'invalid', $errors['post_field_err']);
//            echo $errors['post_field_err'];
        }
    }

    public function deleteAction() {
        if(SessionAuth::isLoggedIn()) {
            $post_id = json_decode(file_get_contents('php://input'), true);
            preg_match('/\d+/', $post_id, $matches);

            $this->postDAO->deletepost($_SESSION['isUserLogin'], $matches[0]);

//            echo json_encode('p'.$matches[0]);
            $this->responseAPI(200, 'ok', 'p'.$matches[0]);
        }
    }

    public function likeAction() {
        if(SessionAuth::isLoggedIn()) {
            $post_id = json_decode(file_get_contents('php://input'), true);
            preg_match('/\d+/', $post_id, $matches);

            if ($this->postDAO->isLike($_SESSION['isUserLogin'], $matches[0]) === 'unliked') {
                $this->postDAO->like($_SESSION['isUserLogin'], $matches[0]);
            } elseif ($this->postDAO->isLike($_SESSION['isUserLogin'], $matches[0]) === 'liked') {
                $this->postDAO->unlike($_SESSION['isUserLogin'], $matches[0]);
            }
            $count_likes = $this->postDAO->getTotalLikes($matches[0]);

//            echo json_encode($count_likes);
            $this->responseAPI(200, 'ok', $count_likes);
        }
    }

//    public function unlikeAction() {
//    if(SessionAuth::isLoggedIn()) {
//        $post_id = json_decode(file_get_contents('php://input'), true);
//        preg_match('/\d+/', $post_id, $matches);
//
//        $this->postDAO->unlike($_SESSION['isUserLogin'], $matches[0]);
//        $count_likes = $this->postDAO->getTotalLikes($matches[0]);
////            echo json_encode($count_likes);
//        $this->responseAPI(200, 'ok', $count_likes);
//    }
//}

}
