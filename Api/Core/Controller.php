<?php

namespace Api\Core;


use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

require_once dirname(__DIR__, 2).'/vendor/autoload.php';

class Controller {

    public function responseAPI(int $statusCode, string $status, $mes) {

        header('Content-type: json/application');

        http_response_code($statusCode);
        $res = [
            'status' => $status,
            'mes' => $mes,
        ];

        echo json_encode($res);
    }
}
