<?php


namespace Api\Service;


interface Auth
{
    public static function logIn($user);

    public static function logOut();

    public static function requireLogIn($entity);

    public static function isLoggedIn();

}
