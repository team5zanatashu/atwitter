<?php


namespace Api\Service;


class SessionAuth implements Auth
{
    public static function logIn($user) {
//        session_start();
        $_SESSION['isUserLogin'] = $user;
//        header('Custom: sessid='.$user);
//        setcookie('sessid', $user);
    }

    public static function logOut() {
//        session_start();
        unset($_SESSION['isUserLogin']);
    }

    public static function isLoggedIn() {
//        session_start();
        if(!empty($_SESSION)) {
            return true;
        }
        return false;
    }

    public static function requireLogIn($entity) {
        if (!self::isLoggedIn()) {
            $entity->responseAPI(401, 'unauthorized', 'logout');
            exit;
        }
    }
}
