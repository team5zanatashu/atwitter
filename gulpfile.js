const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');


gulp.task('sass-compile', () => {
    return gulp.src('./src/scss/**/*-theme.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public/css/'))
});

gulp.task('watch', () => {
    gulp.watch('./src/scss/**/*.scss', gulp.series('sass-compile'))
});